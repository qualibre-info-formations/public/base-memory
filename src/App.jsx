import React, { Component } from 'react'

import './App.css'
import GuessCount from './GuessCount'
import Card from './Card'
import HallOfFame, { HOF_KEY } from './HallOfFame'
import HighScoreInputWrapper from './HighScoreInputWrapper'
import shuffle from 'lodash.shuffle'

const SIDE = 2
const API_KEY = '5d796281b5eedc635380d9b4'
const VISUAL_PAUSE_MSECS = 750

export default class App extends Component {
  state = {
    matchedCardIndices: [],
    currentPair: [],
    cards: [],
    guesses: 0,
    hallOfFame: [],
    scoreStored: false
  }

  handleCardClick = (index) => {
    const { currentPair } = this.state

    if (currentPair.length === 2) {
      return
    }

    if (currentPair.length === 0) {
      this.setState({ currentPair: [index] })
      return
    }

    this.handleNewPairClosedBy(index)
  }

  handleNewPairClosedBy(index) {
    const { cards, currentPair, guesses, matchedCardIndices } = this.state

    const newPair = [currentPair[0], index]
    const newGuesses = guesses + 1
    const matched = cards[newPair[0]] === cards[newPair[1]]
    this.setState({ currentPair: newPair, guesses: newGuesses })
    if (matched) {
      this.setState({ matchedCardIndices: [...matchedCardIndices, ...newPair] })
    }
    setTimeout(() => this.setState({ currentPair: [] }), VISUAL_PAUSE_MSECS)
  }

  getFeedbackForCard(index) {
    const { currentPair, matchedCardIndices } = this.state;
    const indexMatched = matchedCardIndices.includes(index)

    if (currentPair.length < 2) {
      return indexMatched || index === currentPair[0] ? 'visible' : 'hidden'
    }

    if (currentPair.includes(index)) {
      return indexMatched ? 'justMatched' : 'justMismatched'
    }

    return indexMatched ? 'visible' : 'hidden'
  }

  componentDidMount() {
      fetch('https://memory-api.qualibre-formations.fr/cards', {
        headers: {
          'cache-control': 'no-cache',
          'x-apikey': API_KEY,
        },
      })
        .then((result) => {
          if (result.ok) {
            return result.json()
          }
        })
        .then((data) => {
          let symbols = data.cards;
          symbols = shuffle(symbols);
          symbols = symbols.slice(0, (SIDE * SIDE) / 2);
          let newCards = symbols.concat(symbols);
          newCards = shuffle(newCards);
          this.setState({cards: newCards});
        })
        .catch((err) => {
          console.error(err)
        })
    this.setState({hallOfFame: JSON.parse(localStorage.getItem(HOF_KEY) || '[]')});
  }

  render() {
    const { guesses, cards, hallOfFame, matchedCardIndices, scoreStored } = this.state;
    const won = matchedCardIndices.length > 0
      && matchedCardIndices.length === cards.length;
    return (<div className="memory">
      <GuessCount guesses={guesses}/>
      {cards.map((card, index) => (
        <Card
          card={card}
          feedback={this.getFeedbackForCard(index)}
          index={index}
          key={index}
          onClick={this.handleCardClick}
        />
      ))}
      {won &&
        (scoreStored ? (
          <HallOfFame entries={hallOfFame}/>
        ) : (
          <HighScoreInputWrapper
            guesses={guesses}
            onSubmit={(entries) => {
              this.setState({hallOfFame: entries, scoreStored: true})
            }}
          />
        ))}
    </div>);
  }
}