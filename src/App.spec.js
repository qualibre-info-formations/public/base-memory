import { render, screen } from '@testing-library/react';
import App from './App';

const fetchResponse = JSON.stringify([{
  "_id": "5d7789a965b820240000a848",
  "symbol": "😀",
  "_created": "2019-09-10T11:31:53.604Z",
  "_changed": "2019-09-10T11:31:53.604Z",
  "_createdby": "api",
  "_changedby": "api",
  "_version": 0
},
  {
    "_id": "5d7789ba65b820240000a84a",
    "symbol": "💖",
    "_created": "2019-09-10T11:32:10.760Z",
    "_changed": "2019-09-10T11:32:10.760Z",
    "_createdby": "api",
    "_changedby": "api",
    "_version": 0
  },
  {
    "_id": "5d7789cc65b820240000a84b",
    "symbol": "🎉",
    "_created": "2019-09-10T11:32:28.502Z",
    "_changed": "2019-09-10T11:32:28.502Z",
    "_createdby": "api",
    "_changedby": "api",
    "_version": 0
  },
  {
    "_id": "5d7789d865b820240000a84c",
    "symbol": "🍓",
    "_created": "2019-09-10T11:32:40.963Z",
    "_changed": "2019-09-10T14:53:35.317Z",
    "_createdby": "api",
    "_changedby": "api",
    "_version": 1
  },
  {
    "_id": "5d7789e665b820240000a84d",
    "symbol": "🎩",
    "_created": "2019-09-10T11:32:54.062Z",
    "_changed": "2019-09-10T11:32:54.062Z",
    "_createdby": "api",
    "_changedby": "api",
    "_version": 0
  },
  {
    "_id": "5d7789f165b820240000a84f",
    "symbol": "🐶",
    "_created": "2019-09-10T11:33:05.915Z",
    "_changed": "2019-09-10T11:33:05.915Z",
    "_createdby": "api",
    "_changedby": "api",
    "_version": 0
  },
  {
    "_id": "5d7789fe65b820240000a850",
    "symbol": "🐱",
    "_created": "2019-09-10T11:33:18.347Z",
    "_changed": "2019-09-10T11:33:18.347Z",
    "_createdby": "api",
    "_changedby": "api",
    "_version": 0
  },
  {
    "_id": "5d7d2a85832cf56b00001f7c",
    "symbol": "🍿",
    "_created": "2019-09-14T17:59:33.327Z",
    "_changed": "2019-09-14T17:59:33.327Z",
    "_createdby": "api",
    "_changedby": "api",
    "_version": 0
  },
  {
    "_id": "5d7d2a9d832cf56b00001f7d",
    "symbol": "🍟",
    "_created": "2019-09-14T17:59:57.188Z",
    "_changed": "2019-09-14T17:59:57.188Z",
    "_createdby": "api",
    "_changedby": "api",
    "_version": 0
  }
]);
beforeEach(() => {
  fetch.resetMocks();
  fetch.mockResponseOnce(fetchResponse);
});

describe( '<App />', () => {
  test('renders without crashing', () => {
    render(<App />);
  });
  test('renders 0 in App', () => {
    render(<App />);
    expect(screen.getByText(/0/)).toBeInTheDocument();
  });
  test('renders 16 cards after fetching API', async () => {
    render(<App />);
    const cards = await screen.findAllByText(/❓/);
    expect(cards).toHaveLength(16);
  });
})
