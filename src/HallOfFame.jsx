import PropTypes from 'prop-types'
import React, { useState, useEffect } from 'react'
import Geocode from "react-geocode"

import './HallOfFame.css'

const API_KEY = '5d796281b5eedc635380d9b4'

const HallOfFame = ({ entries }) => {
  // This geoloc state will store geocode for all entries
  const [geoloc, setGeoloc] = useState([])
  // Precise coordinates are stored in the API, we want to only show
  // country in the hall of fame
  useEffect(() => {
    async function fetchLoc() {
      Geocode.setApiKey('AIzaSyAFl272ksaGhUJImdAjEbEDcH6rmwPla44');
      const newGeoLoc = [];
      for (let i = 0; i < entries.length; i++) {
        const { coords } = entries[i];
        newGeoLoc[i] = undefined;
        if (coords) {
          const jsonCoords = JSON.parse(coords);
          newGeoLoc[i] = await Geocode.fromLatLng(jsonCoords.latitude, jsonCoords.longitude);
        }
      }
      return newGeoLoc;
    }
    fetchLoc().then(r => setGeoloc(r));
  }, [entries])
  return (
    <table className="hallOfFame">
      <thead>
      <tr>
        <th>Date</th>
        <th>Score</th>
        <th>Player</th>
        <th>Country</th>
      </tr>
      </thead>
      <tbody>
      {entries.map(({id, date, guesses, player}, index) => {
        return (
          <tr key={id}>
            <td className="date">{date}</td>
            <td className="guesses">{guesses}</td>
            <td className="player">{player}</td>
            <td className="coords">
              {geoloc[index] &&
              `${geoloc[index].results[0]['address_components']
                .find(el => el.types.includes('country'))['long_name']}`
              }
            </td>
          </tr>
        )
      })}
      </tbody>
    </table>
  )
}

HallOfFame.propTypes = {
  entries: PropTypes.arrayOf(
    PropTypes.shape({
      date: PropTypes.string.isRequired,
      guesses: PropTypes.number.isRequired,
      id: PropTypes.number.isRequired,
      player: PropTypes.string.isRequired,
      coords: PropTypes.string
    })
  ).isRequired,
}

export default HallOfFame

// == Internal helpers ==============================================

export const HOF_KEY = '::Memory::HallofFame'

export function saveHOFEntry(entry, onStored) {
  entry.date = new Date().toLocaleDateString()
  entry.id = Date.now()

  fetch('https://memoryhalloffame-9cee.restdb.io/rest/halloffame', {
    method: 'POST',
    headers: {
      'cache-control': 'no-cache',
      "Content-Type": "application/json",
      'x-apikey': API_KEY,
    },
    body: JSON.stringify(entry)
  })
    .then((result) => {
      if (result.ok) {
        fetch('https://memoryhalloffame-9cee.restdb.io/rest/halloffame', {
          headers: {
            'cache-control': 'no-cache',
            'x-apikey': API_KEY,
          },
        })
          .then((result) => {
            if (result.ok) {
              return result.json()
            }
          })
          .then((data) => {
            onStored(data);
          })
      }
    })
    .catch((err) => {
      console.error(err);
    })
}
