import PropTypes from 'prop-types'
import React, { Component } from 'react'

import './HighScoreInput.css'

import { saveHOFEntry } from './HallOfFame'

export default class HighScoreInput extends Component {
  state = { winner: '' }

  // Arrow fx for binding
  handleWinnerUpdate = (event) => {
    this.setState({ winner: event.target.value.toUpperCase() })
  }

  // Arrow fx for binding
  persistWinner = (event) => {
    event.preventDefault()
    const newEntry = { guesses: this.props.guesses, player: this.state.winner,
      coords: this.props['coords'] ? JSON.stringify({
        latitude: this.props['coords'].latitude,
        longitude: this.props['coords'].longitude
      }) : undefined }
    saveHOFEntry(newEntry, (entries) => {
      this.props.onSubmit(entries);
    })
  }

  render() {
    return (
      <div>
        <form className="highScoreInput" onSubmit={this.persistWinner}>
          <p>
            <label>
              Bravo! Entre ton prénom :
              <input
                autoComplete="given-name"
                type="text"
                onChange={this.handleWinnerUpdate}
                value={this.state.winner}
              />
            </label>
            <button type="submit">J’ai gagné !</button>
          </p>
        </form>
        <div>Localisation : {this.props['coords'] && this.props['coords'].latitude},
          {' '}{this.props['coords'] && this.props['coords'].longitude}</div>
    </div>
    )
  }
}

HighScoreInput.propTypes = {
  guesses: PropTypes.number.isRequired,
  onSubmit: PropTypes.func
}
