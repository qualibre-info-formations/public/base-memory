import React from 'react'
import HighScoreInput from './HighScoreInput'
import { geolocated } from 'react-geolocated'

const HighScoreInputWrapper = (props) => (
  <HighScoreInput {...props} />
)

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(HighScoreInputWrapper)